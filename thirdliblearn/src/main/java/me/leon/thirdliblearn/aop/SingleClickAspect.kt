package me.leon.thirdliblearn.aop

import android.util.Log
import android.view.View
import me.leon.thirdliblearn.BuildConfig
import me.leon.thirdliblearn.R
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import java.util.*

/**
 * @author : Leon Shih
 * @time   : 2019/3/14 0014 18:47
 * @e-mail : deadogone@gmail.com    :
 * @desc   :
 */
@Aspect
class SingleClickAspect {
    @Pointcut("execution(@me.leon.thirdliblearn.aop.SingleClick * *(..))") //方法切入点
    fun methodAnnotated() {

    }

    @Around("methodAnnotated()")//在连接点进行方法替换
    @Throws(Throwable::class)
    fun aroundJoinPoint(joinPoint: ProceedingJoinPoint) {
        var view: View? = null
        for (arg in joinPoint.args) {
            if (arg is View) view = arg
        }
        if (view != null) {
            val tag = view.getTag(TIME_TAG)
            val lastClickTime = if (tag != null) tag as Long else 0
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "lastClickTime:" + lastClickTime)
            }
            val currentTime = Calendar.getInstance().timeInMillis
            if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {//过滤掉600毫秒内的连续点击
                view.setTag(TIME_TAG, currentTime)
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "currentTime:" + currentTime)
                }
                joinPoint.proceed()//执行原方法
            }
        }
    }

    companion object {

        const val TAG = "SingleClickAspect"
        const val MIN_CLICK_DELAY_TIME = 1000
        internal var TIME_TAG = R.id.click_time
    }
}