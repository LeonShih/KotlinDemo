package me.leon.thirdliblearn.aop

/**
 * @author : Leon Shih
 * @time   : 2019/3/14 0014 18:45
 * @e-mail : deadogone@gmail.com    :
 * @desc   :
 */
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
annotation class SingleClick