package me.leon.thirdliblearn

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import me.leon.thirdliblearn.aop.SingleClick
import org.koin.androidx.viewmodel.ext.viewModel

class MainActivity : AppCompatActivity() {
    val vm: FirstViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    @SingleClick
    fun hello(view: View) {
        vm.sayHello()
    }
    fun hello2(view: View) {
        vm.sayHello2()
    }
}
