package me.leon.thirdliblearn

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * @author : Leon Shih
 * @time   : 2019/3/11 0011 16:49
 * @e-mail : deadogone@gmail.com    :
 * @desc   :
 */

val appModule = module {
    viewModel { FirstViewModel() }
}