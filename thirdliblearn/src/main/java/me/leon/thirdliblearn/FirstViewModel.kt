package me.leon.thirdliblearn

import androidx.lifecycle.ViewModel

class FirstViewModel : ViewModel() {
    fun sayHello() = println("Hello 1st - $this")
    fun sayHello2() = println("Hello2 1st - $this")
}