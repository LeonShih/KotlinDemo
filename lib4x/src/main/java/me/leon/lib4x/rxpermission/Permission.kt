package me.leon.lib4x.rxpermission

/**
 * Author : Leon Shih
 * Time   :2018/9/13 0013 14:29
 * E-mail :deadogone@gmail.com    :
 * Desc   :Permission data class
 */

data class Permission @JvmOverloads internal constructor
                    (val name: String,
                     val granted: Boolean,
                     val shouldShowRequestPermissionRationale: Boolean = false)
