package me.leon.lib4x.rxbus

/**
 * RxBus data
 * Created by gorden on 2016/7/8.
 */
class BusData @JvmOverloads constructor(var id: String = "", var status: String = "")
