package me.leon.lib4x.rxbus

import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

/**
 *
 * Created by gorden on 2016/7/23.
 */
class SubscriberMethod(var subscriber: Any, var method: Method,
                       var eventType: Class<*>, var code: Int,
                       var threadMode: ThreadMode,
                       var isSticky: Boolean =false
) {


    /**
     * 调用方法
     * @param o 参数
     */
     fun invokes(o: Any) {
        try {
            val parameterType = method.parameterTypes
            if (parameterType != null && parameterType.size == 1) {
                method.invoke(subscriber, o)
            } else if (parameterType == null || parameterType.size == 0) {
                method.invoke(subscriber)
            }
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }

    }
}
