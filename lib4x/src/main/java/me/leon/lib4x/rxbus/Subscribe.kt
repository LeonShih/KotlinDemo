package me.leon.lib4x.rxbus

import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * Rxbus
 * Created by gorden on 2016/7/23.
 */
@Documented
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(RetentionPolicy.RUNTIME)
annotation class Subscribe(val code: Int = -1,
                           val threadMode: ThreadMode = ThreadMode.CURRENT_THREAD,
                           val isSticky : Boolean =false

)
