package me.leon.lib4x.rxbus

/**
 *
 * Created by gorden on 2016/7/23.
 */
enum class ThreadMode {
    /**
     * current thread
     */
    CURRENT_THREAD,

    /**
     * android main thread
     */
    MAIN,


    /**
     * new thread
     */
    NEW_THREAD
}
