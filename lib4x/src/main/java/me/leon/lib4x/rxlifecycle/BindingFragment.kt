package me.leon.lib4x.rxlifecycle

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.reactivex.subjects.BehaviorSubject

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 14:32
 * E-mail :deadogone@gmail.com    :
 * Desc   : BindingFragment bind lifecycle
 */
class BindingFragment : Fragment() {

    val mLifeCycleBehavior = BehaviorSubject.create<Life>()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mLifeCycleBehavior.onNext(Life.ATTACH)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLifeCycleBehavior.onNext(Life.CREATE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mLifeCycleBehavior.onNext(Life.CREATE_VIEW)
        return null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mLifeCycleBehavior.onNext(Life.ACTIVITY_CREATE)
    }

    override fun onStart() {
        super.onStart()
        mLifeCycleBehavior.onNext(Life.START)
    }

    override fun onResume() {
        super.onResume()
        mLifeCycleBehavior.onNext(Life.RESUME)
    }

    override fun onPause() {
        super.onPause()
        mLifeCycleBehavior.onNext(Life.PAUSE)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mLifeCycleBehavior.onNext(Life.DESTROY_VIEW)
    }

    override fun onDestroy() {
        super.onDestroy()
        mLifeCycleBehavior.onNext(Life.DESTROY)
    }

    override fun onDetach() {
        super.onDetach()
        mLifeCycleBehavior.onNext(Life.DETACH)
    }

}