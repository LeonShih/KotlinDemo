package me.leon.lib4x.rxlifecycle

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 14:35
 * E-mail :deadogone@gmail.com    :
 * Desc   : fragment生命周期Enum
 */
enum class Life {
    ATTACH,
    CREATE,
    CREATE_VIEW,
    ACTIVITY_CREATE,
    START,
    RESUME,
    PAUSE,
    STOP,
    DESTROY_VIEW,
    DESTROY,
    DETACH
}