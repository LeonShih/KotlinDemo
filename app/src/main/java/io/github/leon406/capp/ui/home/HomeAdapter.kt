package io.github.leon406.capp.ui.home

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder


class HomeAdapter(var datas: MutableList<Int>)
    : BaseQuickAdapter<Int, BaseViewHolder>(android.R.layout.simple_list_item_1, datas) {
    override fun convert(holder: BaseViewHolder, item: Int) {
        holder.setText(android.R.id.text1, "hello $item")
        holder.addOnClickListener(android.R.id.text1)
    }

}