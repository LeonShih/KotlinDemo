package io.github.leon406.capp.ui.other

import io.github.leon406.capp.R
import io.github.leon406.capp.base.mvp.MvpActivity
import io.github.leon406.capp.ui.TestContract
import io.github.leon406.capp.ui.TestPresenter
import io.reactivex.Flowable
import kotlinx.android.synthetic.main.activity_other.*
import me.leon.lib4x.rxlifecycle.RxLifecycle
import java.util.concurrent.TimeUnit

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 17:30
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
class OtherActivity : MvpActivity<TestContract.View, TestPresenter>(), TestContract.View {
    override fun onViewInit() {
        tv_other.text = "Another Page"
        tv_other.setOnClickListener {
            getPresenter().doHello()
        }

        back.setOnClickListener {
            finish()
        }

        getPresenter().fetch()
    }

    override fun bindLayout() = R.layout.activity_other

    override fun bindPresenter() = TestPresenter()

    override fun onHello() {
        Flowable.interval(1, TimeUnit.SECONDS)
                .compose(RxLifecycle.with(this).bindToDestroy())
                .subscribe {
                    println("current time  $it")

                }
    }
}