package io.github.leon406.capp.ui.other

import com.lzy.okgo.model.HttpParams
import io.github.leon406.capp.MockApi
import io.github.leon406.capp.base.BasePresenter
import io.github.leon406.capp.bean.MeiZi
import io.github.leon406.capp.engine.net.OkRx2Manager
import me.leon.lib4x.log.Leon

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 13:42
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
class OtherPresenter : BasePresenter<OtherContract.View>(), OtherContract.Presenter {


    override fun fetch(page: Int) {

        OkRx2Manager.get(MockApi.MEIZI + page, HttpParams(), MeiZi::class.java)
                .subscribe({
                    Leon.d("Resp", it.toString())
                    getView().onFetch(it)
                }, { t ->
                    println(t.cause?.message)

                    getView().onError(t)
                })
    }
}