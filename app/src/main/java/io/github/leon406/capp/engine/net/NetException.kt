package io.github.leon406.capp.engine.net

class NetException(val msg: String, val throwable: Throwable) :Exception(msg,throwable)