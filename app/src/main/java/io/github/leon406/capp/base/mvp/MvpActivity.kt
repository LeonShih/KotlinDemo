package io.github.leon406.capp.base.mvp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 11:57
 * E-mail :deadogone@gmail.com    :
 * Desc   :Mvp Activity base class
 */
abstract class MvpActivity<V : IView, P : IPresenter<V>> : AppCompatActivity() {
    lateinit var mPresenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        screenAutoAdapt(1080f)
        mPresenter = bindPresenter()
        mPresenter.attachView(this as V)
        setContentView(bindLayout())
        onViewInit()
    }

    fun screenAutoAdapt(designWidth: Float) {

        val displayMetrics = getResources().getDisplayMetrics()
        val density = displayMetrics.widthPixels / designWidth

        displayMetrics.density = density
        displayMetrics.densityDpi = (displayMetrics.density * 160).toInt()

    }

    abstract fun onViewInit()

    abstract fun bindLayout(): Int

    abstract fun bindPresenter(): P
    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

    fun getPresenter(): P = mPresenter
    open fun onError(throwable: Throwable) {}
}