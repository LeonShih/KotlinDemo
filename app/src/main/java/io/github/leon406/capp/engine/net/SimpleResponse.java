package io.github.leon406.capp.engine.net;

import java.io.Serializable;

/**
 * Author:  Leon
 * Time:    2017/4/6 下午2:41
 * Desc:    网络请求数据类
 */

public class SimpleResponse implements Serializable {

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    @Deprecated
    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }


    @Deprecated
    public void setMessage(String message) {
        this.message = message;
    }
    public HTTPResponse toHTTPResponse() {
        HTTPResponse httpResponse = new HTTPResponse(code,message,null);
        return httpResponse;
    }

}
