package io.github.leon406.capp.base

import io.github.leon406.capp.base.mvp.IPresenter
import io.github.leon406.capp.base.mvp.IView
import java.lang.ref.WeakReference

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 11:39
 * E-mail :deadogone@gmail.com    :
 * Desc   : MVP-- Presenter 持有View的引用
 */
abstract class BasePresenter<V : IView> : IPresenter<V> {
    lateinit var mView: WeakReference<V>

    override fun attachView(v: V) {
        mView = WeakReference(v)
    }

    override fun detachView() {
        mView.clear()
    }

    override fun getView() = mView.get() ?: throw(NullPointerException("view is null"))
}