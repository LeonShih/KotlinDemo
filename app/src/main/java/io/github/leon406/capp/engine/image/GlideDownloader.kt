package io.github.leon406.capp.engine.image

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import io.github.leon406.capp.R
import java.io.File

/**
 * Author : Leon Shih
 * Time   :2018/8/14 0014 10:11
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
class GlideDownloader : ImageDownloader {
    override fun displayImage(ctx: Context, path: Any, view: ImageView) {

        when (path) {
            is String -> {
                GlideApp.with(ctx).load(path)
                        .dontAnimate()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .override(view.width / 2, view.height / 2)
                        .into(view)
            }
            is Uri -> {
                GlideApp.with(ctx).load(path)
                        .dontAnimate()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .override(view.width / 2, view.height / 2)
                        .into(view)
            }
            is File -> {
                GlideApp.with(ctx).load(path)
                        .dontAnimate()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .override(view.width / 2, view.height / 2)
                        .into(view)
            }
            is Int -> {
                GlideApp.with(ctx).load(path)
                        .dontAnimate()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .override(view.width / 2, view.height / 2)
                        .into(view)
            }
            is Drawable -> {
                GlideApp.with(ctx).load(path)
                        .dontAnimate()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .override(view.width / 2, view.height / 2)
                        .into(view)
            }
        }
    }
}