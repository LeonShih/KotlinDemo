package io.github.leon406.capp.base.mvp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment


/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 12:09
 * E-mail :deadogone@gmail.com    :
 * Desc   : Mvp  Fragment  base class
 */
abstract class MvpFragment<V : IView, P : IPresenter<V>> : Fragment() {

    lateinit var mPresenter: P
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(bindLayout(), container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onViewInit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter = bindPresenter()
        mPresenter.attachView(this as V)
    }

    abstract fun onViewInit()

    abstract fun bindLayout(): Int

    abstract fun bindPresenter(): P
    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

    fun getPresenter(): P = mPresenter

    open fun onError(throwable: Throwable) {}
}