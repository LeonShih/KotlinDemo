package io.github.leon406.capp.ui.other

import io.github.leon406.capp.base.mvp.IPresenter
import io.github.leon406.capp.base.mvp.IView
import io.github.leon406.capp.bean.MeiZi

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 13:40
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
class OtherContract {
    interface View : IView {
        fun onFetch(meiZi: MeiZi)
    }

    interface Presenter : IPresenter<View> {
        fun fetch(page: Int)
    }
}