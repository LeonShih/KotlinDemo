package io.github.leon406.capp.ui.home

import androidx.recyclerview.widget.LinearLayoutManager
import io.github.leon406.capp.R
import io.github.leon406.capp.base.mvp.LCEMvpFragment
import io.github.leon406.capp.extension.timer
import io.github.leon406.capp.extension.toast
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : LCEMvpFragment<HomeContract.View, HomePresenter>(), HomeContract.View {
    override fun onContentViewInit() {
        showLoading()
        rv.layoutManager = LinearLayoutManager(activity!!)

        srl.setOnRefreshListener {

        }


        val adapter = HomeAdapter(arrayListOf(1, 2, 3, 4, 5, 6, 7, 8, 1, 23, 4, 5, 6, 6, 7, 8))
        adapter.setOnLoadMoreListener {
            activity?.toast("loadMore")
            5.timer().subscribe {
                adapter.loadMoreEnd()
            }
        }
        srl.setOnRefreshListener {

            adapter.add(0,888)
            adapter.notifyDataSetChanged()
            srl.isRefreshing = false


        }
        adapter.setOnItemChildClickListener { adapter, view, position ->
            activity?.toast(" children click ${view.id == android.R.id.text1}")
        }
        rv.adapter = adapter

//        adapter.itemClickListener = object: OnRecyclerViewItemClickListener {
//            override fun onItemClick(v: View, position: Int) {
//                activity?.toast(position.toString())
//                adapter.add(adapter.itemCount,999)
////                adapter.addDatas(arrayListOf(12, 22, 32, 42, 22, 61, 27, 88))
////                adapter.setNewDatas(arrayListOf(12, 22, 32, 42, 22, 61, 27, 88))
//            }
//        }
        getPresenter().fetch()
    }

    override fun bindContent() = R.layout.fragment_home

    //    override fun bindLayout() = R.layout.fragment_home
    override fun bindPresenter() = HomePresenter()

    override fun onHello() {
        showContent()
        activity?.toast("Hello")
    }

    override fun onError(throwable: Throwable) {
        showError()
    }

}