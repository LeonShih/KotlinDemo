package io.github.leon406.capp

/**
 * Author : Leon Shih
 * Time   :2018/8/10 0010 15:08
 * E-mail :deadogone@gmail.com    :
 * Desc   : mock api
 */
class MockApi {
    companion object {
        private val API = "https://dsn.apizza.net/mock/16c9e14afa9625ca49ee27526c1efdb5/"
        val UPDATE = API + "update"
        val HOME = API + "home"
        val MEIZI = "https://gank.io/api/data/%E7%A6%8F%E5%88%A9/10/"
    }

}