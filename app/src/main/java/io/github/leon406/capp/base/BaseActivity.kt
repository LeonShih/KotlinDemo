package io.github.leon406.capp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


/**
 * Author : Leon Shih
 * Time   :2018/8/10 0010 14:26
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenAutoAdapt(360f)
        setContentView(bindLayout())
        initView()
        doBiz()

    }

    /**
     * 屏幕宽度自动适配
     * @param designWidth 设计图宽度
     */
    private fun screenAutoAdapt(designWidth: Float) {

        val displayMetrics = resources.displayMetrics
        val density = displayMetrics.widthPixels / designWidth

        displayMetrics.density = density
        displayMetrics.densityDpi = (displayMetrics.density * 160).toInt()
        displayMetrics.scaledDensity = density

    }

    abstract fun doBiz()

    abstract fun initView()

    abstract fun bindLayout(): Int
}