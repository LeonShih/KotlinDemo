package io.github.leon406.capp.ui.preview

import io.github.leon406.capp.R
import io.github.leon406.capp.base.mvp.MvpActivity
import io.github.leon406.capp.engine.image.ImageLoader
import io.github.leon406.capp.ui.TestContract
import io.github.leon406.capp.ui.TestPresenter
import kotlinx.android.synthetic.main.activity_photo_preview.*

class PhotoPreviewActivity : MvpActivity<TestContract.View, TestPresenter>(),TestContract.View {
    override fun onHello() {
    }

    override fun onViewInit() {
        val url = intent.getStringExtra("url")
        ImageLoader.displayImage(this, url, photo)
        photo.setOnPhotoTapListener { _, _, _ -> finish() }
    }

    override fun bindLayout() = R.layout.activity_photo_preview

    override fun bindPresenter(): TestPresenter {
        return TestPresenter()
    }
}