package io.github.leon406.capp.ui.other

import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import io.github.leon406.capp.R
import io.github.leon406.capp.bean.Result
import io.github.leon406.capp.engine.image.ImageLoader


class MeiziAdapter(var datas: MutableList<Result>)
    : BaseQuickAdapter<Result, BaseViewHolder>(R.layout.item_meizi, datas) {


    override fun convert(holder: BaseViewHolder, item: Result) {

        ImageLoader.displayImage(mContext, item.url, holder.getView<ImageView>(R.id.iv))
        holder.addOnClickListener(R.id.iv)
    }

}