package io.github.leon406.capp

import android.app.Application
import android.content.Context
import io.github.leon406.capp.engine.image.GlideDownloader
import io.github.leon406.capp.engine.image.ImageLoader
import io.github.leon406.capp.engine.net.OkRx2Manager
import me.leon.lib4x.log.Leon


/**
 * Author : Leon Shih
 * Time   :2018/8/10 0010 14:17
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
class App : Application() {

    companion object {
        @JvmStatic
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        ImageLoader.init(GlideDownloader())

        OkRx2Manager.init(this)

        Leon.apply {
            setDebug(true)
            setPrint(true)
            setPrintFile(false)
            setMaxLogFileLength(10 * 1024 * 1024)
//            setLogFilePath(Environment.getExternalStorageDirectory().absolutePath + "/Leon/Log.txt")
        }
    }
}