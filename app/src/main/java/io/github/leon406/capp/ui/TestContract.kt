package io.github.leon406.capp.ui

import io.github.leon406.capp.base.mvp.IPresenter
import io.github.leon406.capp.base.mvp.IView

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 13:40
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
class TestContract {
    interface View : IView {
        fun onHello()
    }

    interface Presenter : IPresenter<View> {
        fun doHello()
    }
}