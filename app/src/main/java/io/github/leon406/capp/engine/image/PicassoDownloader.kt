package io.github.leon406.capp.engine.image

import android.content.Context
import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.io.File

/**
 * Author : Leon Shih
 * Time   :2018/8/14 0014 9:58
 * E-mail :deadogone@gmail.com    :
 * Desc   : Picasso Loader
 */
class PicassoDownloader : ImageDownloader {
    override fun displayImage(ctx: Context, path: Any, view: ImageView) {
        when (path) {
            is String -> {
                println("$path")
                Picasso.get().load(path).into(view)
            }
            is Uri -> {
                Picasso.get().load(path).into(view)
            }
            is File -> {
                Picasso.get().load(path).into(view)
            }
            is Int -> {
                Picasso.get().load(path).into(view)
            }
            else -> {
                throw IllegalArgumentException("wrong type")
            }
        }
    }
}