package io.github.leon406.capp.engine.net

/**
 * Author:  Leon
 * Time:    2017/4/6 下午2:41
 * Desc:    网络请求数据类
 */

data class  HTTPResponse<T>(var code:Int=0,var message:String?=null,var data: T)