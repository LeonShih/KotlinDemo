package io.github.leon406.capp.engine.image

import android.content.Context
import android.widget.ImageView

/**
 * Author : Leon Shih
 * Time   :2018/8/14 0014 9:44
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
object ImageLoader {
    lateinit var downloader: ImageDownloader
    fun init(downloader: ImageDownloader) {
        ImageLoader.downloader = downloader
    }

    fun displayImage(ctx: Context, path: Any, view: ImageView) {
        downloader.displayImage(ctx, path, view)
    }
}