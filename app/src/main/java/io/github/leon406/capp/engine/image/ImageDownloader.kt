package io.github.leon406.capp.engine.image

import android.content.Context
import android.widget.ImageView

/**
 * Author : Leon Shih
 * Time   :2018/8/14 0014 9:38
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
interface ImageDownloader {
    fun displayImage(ctx: Context, path: Any, view: ImageView)
}