package io.github.leon406.capp.ui.msg

import io.github.leon406.capp.R
import io.github.leon406.capp.base.BaseFragment
import io.github.leon406.capp.extension.toast
import kotlinx.android.synthetic.main.fragment_test.*

class MsgFragment : BaseFragment() {
    override fun bindLayout() = R.layout.fragment_test

    override fun initView() {

        tv_home.apply {
            setOnClickListener {
                activity?.toast("Hello")
            }
            text = "MsgFragment"
        }

    }
}