package io.github.leon406.capp.base.mvp

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 11:37
 * E-mail :deadogone@gmail.com    :
 * Desc   : MVP--P 层 负责与Model和View交互
 */
interface IPresenter<V : IView> {
    fun getView(): V
    fun attachView(v: V)
    fun detachView()
}