package io.github.leon406.capp.ui

import android.Manifest
import android.os.SystemClock
import io.github.leon406.capp.R
import io.github.leon406.capp.base.BaseActivity
import io.github.leon406.capp.ui.home.HomeFragment
import io.github.leon406.capp.ui.mine.MineFragment
import io.github.leon406.capp.ui.msg.MsgFragment
import io.github.leon406.capp.ui.other.OtherFragment
import io.reactivex.Flowable
import kotlinx.android.synthetic.main.activity_main.*
import me.leon.lib4x.log.Leon
import me.leon.lib4x.rxlifecycle.RxLifecycle
import me.leon.lib4x.rxpermission.RxPermissions

class MainActivity : BaseActivity() {

    override fun bindLayout(): Int = R.layout.activity_main
    override fun initView() {
        bottom.apply {
            containerId = R.id.fl_container
            isHideText = false
            setTitleBeforeAndAfterColor("#999999", "#ff5d5e")
            addItem(HomeFragment::class.java, "首页",
                    R.mipmap.comui_tab_home, R.mipmap.comui_tab_home_selected)
            addItem(MsgFragment::class.java, "消息",
                    R.mipmap.comui_tab_message, R.mipmap.comui_tab_message_selected)
            addItem(OtherFragment::class.java, "其他",
                    R.mipmap.comui_tab_pond, R.mipmap.comui_tab_pond_selected)
            addItem(MineFragment::class.java, "我的",
                    R.mipmap.comui_tab_person, R.mipmap.comui_tab_person_selected)
        }.build()


        Leon.ll("Hello", "66666")

        val rxPermissions = RxPermissions(this)
        rxPermissions.request(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION

        )
                .subscribe{
                    if (it) {
                        Leon.d("permission granted")
                    }else{
                        Leon.d("permission refused")
                    }
                }


    }

    override fun doBiz() {

        Flowable.fromCallable {

            SystemClock.sleep(10000)

            resources.displayMetrics.density
            resources.displayMetrics.densityDpi

            2
        }
                .compose(RxLifecycle.rxSwitch())
                .subscribe {
                    //                    println(Thread.currentThread().name)
//                    toast("It's time to show!")

                }
    }
}
