package io.github.leon406.capp.base.mvp

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 11:37
 * E-mail :deadogone@gmail.com    :
 * Desc   : MVP--View 层
 */
interface IView {
    fun onError(throwable: Throwable)
}