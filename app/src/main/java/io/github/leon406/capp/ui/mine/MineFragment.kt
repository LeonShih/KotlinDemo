package io.github.leon406.capp.ui.mine


import io.github.leon406.capp.R
import io.github.leon406.capp.base.BaseFragment
import io.github.leon406.capp.extension.toast
import kotlinx.android.synthetic.main.fragment_test.*
import me.leon.lib4x.rxbus.RxBus
import me.leon.lib4x.rxbus.Subscribe
import me.leon.lib4x.rxbus.ThreadMode

class MineFragment : BaseFragment() {
    override fun bindLayout() = R.layout.fragment_test

    override fun initView() {

        tv_home.apply {
            setOnClickListener {
                activity?.toast("MineFragment")
            }
            text = "MineFragment"
        }

        RxBus.default.register(this )
    }


    @Subscribe(-1, ThreadMode.NEW_THREAD,true)
    fun hello2(msg : Boolean){
        println("${Thread.currentThread().name} + $msg ")
    }
}