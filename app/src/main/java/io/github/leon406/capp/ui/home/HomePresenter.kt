package io.github.leon406.capp.ui.home

import com.lzy.okgo.model.HttpParams
import io.github.leon406.capp.MockApi
import io.github.leon406.capp.base.BasePresenter
import io.github.leon406.capp.bean.HomeBean
import io.github.leon406.capp.engine.net.OkRx2Manager
import me.leon.lib4x.log.Leon

/**
 * Author : Leon Shih
 * Time   :2018/8/13 0013 13:42
 * E-mail :deadogone@gmail.com    :
 * Desc   :
 */
class HomePresenter : BasePresenter<HomeContract.View>(), HomeContract.Presenter {
    override fun doHello() {
        getView().onHello()
    }

    fun fetch() {

        OkRx2Manager.getData(MockApi.HOME, HttpParams(), HomeBean.DataBean::class.java)
                .subscribe({
                    Leon.d("Resp", it.head.ads.get(0))
                    getView().onHello()
                }, { t -> println(t.cause?.message)

                        getView().onError(t)
                })
    }
}