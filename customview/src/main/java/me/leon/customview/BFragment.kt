package com.shiduai.mobile.lawyer.frame

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

/**
 * <p>description：base Fragment  </p>
 * <p>author：Leon</p>
 * <p>date：2019/2/14 0014</p>
 * <p>e-mail：deadogone@gmail.com</p>
 *
 */
abstract class BFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(bindLayout(), container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onViewInit()
    }
    abstract fun onViewInit()
    abstract fun bindLayout(): Int
}