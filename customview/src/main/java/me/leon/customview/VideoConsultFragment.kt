package com.shiduai.mobile.lawyer.ui.history

import com.bigkoo.pickerview.builder.TimePickerBuilder
import com.bigkoo.pickerview.listener.OnTimeSelectListener
import com.shiduai.mobile.lawyer.frame.BFragment
import kotlinx.android.synthetic.main.fragment_video_consult.*
import me.leon.customview.R
import java.text.SimpleDateFormat
import java.util.*
import me.leon.customview.CityPicker


/**
 * <p>description：</p>
 * <p>author：Leon</p>
 * <p>date：2019/2/14 0014</p>
 * <p>e-mail：deadogone@gmail.com</p>
 *
 */
class VideoConsultFragment : BFragment() {
    override fun onViewInit() {

        sivName.setOnClickListener {
            sivName.setRightTxt("")
            sivName.setRightImg(R.drawable.abc_ic_arrow_drop_right_black_24dp)
        }
        sivDate.setOnClickListener {
            initTimePicker()
        }
        sivLocation.setOnClickListener {
            CityPicker().showCityPickerView(context)
        }

    }


    private fun initTimePicker() {
        val endDate = Calendar.getInstance()
        endDate.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR) )

        val startDate = Calendar.getInstance()
        startDate.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR) - 40)

        //时间选择器
        val pvTime = TimePickerBuilder(context,
                OnTimeSelectListener { date, v ->
                    sivDate.setRightTxt(SimpleDateFormat("yyyy-MM-dd").format(date))
                })
                .setRangDate(startDate,endDate)//起始终止年月日设定
                .build()

        pvTime.show()
    }

    override fun bindLayout(): Int = R.layout.fragment_video_consult
}