package com.shiduai.mobile.lawyer.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF

import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import me.leon.customview.R


/**
 * <p>description：</p>
 * <p>author：Leon</p>
 * <p>date：2019/2/14 0014</p>
 * <p>e-mail：deadogone@gmail.com</p>
 *
 */
class IndicatorView @JvmOverloads constructor(context: Context, attrs: AttributeSet?, defaultStyle: Int = 0) : View(context, attrs, defaultStyle) {
    var mSelectColor = Color.parseColor("#FFFFFF")
    var mPaint: Paint = Paint().apply {
        isDither = true
        isAntiAlias = true
        style = Paint.Style.FILL_AND_STROKE
    }
    var mCount = 3
    var mRadius = 64
    var mDocUnselectColor = -0xf0f001
    var mSpace = 50
    var mSelectPosition = 0
    var mIndicators: MutableList<PointF> = mutableListOf()
    var mPositonChangeListener: PositionChangeListener? = null
    var recyclerView: RecyclerView? = null

    init {
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6f, resources.getDisplayMetrics());
        val a = context.obtainStyledAttributes(attrs, R.styleable.IndicatorView);
        mRadius = a.getDimensionPixelSize(R.styleable.IndicatorView_indicatorRadius, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6f, resources.getDisplayMetrics()).toInt());
        mSpace = a.getDimensionPixelSize(R.styleable.IndicatorView_indicatorSpace, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.getDisplayMetrics()).toInt());

        mSelectColor = a.getColor(R.styleable.IndicatorView_indicatorSelectColor, Color.RED);
        mDocUnselectColor = a.getColor(R.styleable.IndicatorView_indicatorColor, Color.GRAY);
        a.recycle();
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = (mRadius) * 2 * mCount + mSpace * (mCount - 1)
        val height = mRadius * 2 + mSpace * 2
        setMeasuredDimension(width, height)
        measureIndicator()
    }

    private fun measureIndicator() {
        mIndicators.clear()
        var cx = 0
        for (i in 0 until mCount) {
            val indicator = PointF()
            if (i == 0) {
                cx = mRadius
            } else {
                cx += mRadius * 2 + mSpace
            }
            indicator.x = cx.toFloat()
            indicator.y = (measuredHeight / 2).toFloat()
            mIndicators.add(indicator)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        for (i in mIndicators.indices) {

            val indicator = mIndicators[i]
            val x = indicator.x
            val y = indicator.y

            if (mSelectPosition == i) {
                mPaint.style = Paint.Style.FILL
                mPaint.color = mSelectColor
            } else {
                mPaint.color = mDocUnselectColor
                mPaint.style = Paint.Style.FILL
            }
            canvas?.drawCircle(x, y, mRadius.toFloat(), mPaint)
        }
    }

//    override fun onTouchEvent(event: MotionEvent?): Boolean {
//
//        when (event?.action) {
//            MotionEvent.ACTION_DOWN -> {
//                handleActionDown(event.x, event.y)
//            }
//        }
//        return super.onTouchEvent(event)
//    }
//
//    private fun handleActionDown(xDis: Float, yDis: Float) {
//        for (i in mIndicators.indices) {
//            val indicator = mIndicators[i]
//            val rectF = RectF(indicator.x - (mRadius), yDis - (indicator.y), indicator.x + mRadius.toFloat(), indicator.y + mRadius.toFloat())
//            if (rectF.contains(xDis, yDis)) {
//                println("change indicator")
//            }
//            if (xDis < indicator.x + mRadius.toFloat().toFloat()
//                    && xDis >= indicator.x - (mRadius)
//                    && yDis >= yDis - (indicator.y)
//                    && yDis < indicator.y + mRadius.toFloat().toFloat()) {
//                setCurrent(i)
//                break
//            }
//        }
//    }

    fun setUpWithRecyclerView(rv: RecyclerView) {
        recyclerView = rv
        mCount = rv.adapter?.itemCount!!
        rv.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (rv.layoutManager is LinearLayoutManager) {
                val position = (rv.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                if (position != -1 && mSelectPosition != position) {
                    mPositonChangeListener?.let {
                        mPositonChangeListener!!.onPositionChange(mSelectColor, position)
                    }
                    setCurrent(position)
                    Log.d("recycler", "visiable $position")
                }
            }
        }
    }

    fun setCurrent(index: Int) {
        mSelectPosition = index
        invalidate()
    }

    fun setListener(listener: PositionChangeListener) {
        mPositonChangeListener = listener
    }

    interface PositionChangeListener {
        fun onPositionChange(old: Int, new: Int)
    }
}