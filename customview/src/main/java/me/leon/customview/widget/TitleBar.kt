package com.shiduai.mobile.lawyer.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.layout_title.view.*
import me.leon.customview.R

class TitleBar @JvmOverloads constructor (context: Context, attrs: AttributeSet? = null) : RelativeLayout(context, attrs) {
    init {
        var root = View.inflate(context, R.layout.layout_title, this)
        initStyle(attrs)
    }

    private fun initStyle(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.TitleBar)

        val left = a.getResourceId(R.styleable.TitleBar_left_icon, -1)
        val right = a.getResourceId(R.styleable.TitleBar_right_icon, -1)
        val title = a.getString(R.styleable.TitleBar_title)
        val title2 = a.getResourceId(R.styleable.TitleBar_title, -1)
        val rightTxt = a.getString(R.styleable.TitleBar_right_text)
        val rightTxt2 = a.getResourceId(R.styleable.TitleBar_right_text, -1)
        if (left != -1) setLeftIcon(left)
        if (right != -1) setRightIcon(right)
        if (title2 != -1) setTitle(title2)
        if (!title.isNullOrEmpty()) setTitle(title)
        if (rightTxt2 != -1) setRightTxt(rightTxt2)
        if (!rightTxt.isNullOrEmpty()) setRightTxt(rightTxt)
        a.recycle()
    }

    private fun setRightTxt(txt: Int) {
      iv_right.setText(txt)
    }
    private fun setRightTxt(txt: String) {
        iv_right.setText(txt)
    }

    fun setLeftIcon(resInt: Int) = iv_left.setBackgroundResource(resInt)
    fun setRightIcon(resInt: Int) = iv_right.setBackgroundResource(resInt)
    fun setTitle(resInt: Int) = tv_title.setText(resInt)
    fun setTitle(resInt: String) = tv_title.setText(resInt)

    fun getLeftIconView() = iv_left
    fun setRightIconView() = iv_right

}
