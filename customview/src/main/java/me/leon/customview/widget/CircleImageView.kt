package me.leon.customview.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.RelativeLayout
import me.leon.customview.R

class CircleImageView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ImageView(context, attrs) {
    init {
        initStyle(attrs)
    }
    private val path = Path()
    private fun initStyle(attrs: AttributeSet?) {
//        val a = context.obtainStyledAttributes(attrs, R.styleable.SettingItemView)
//
//        a.recycle()
//
    }

    override fun onDraw(canvas: Canvas?) {
        path.addRoundRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), width.toFloat() / 2, width.toFloat() / 2, Path.Direction.CW)
        canvas?.clipPath(path)
        super.onDraw(canvas)
    }
}