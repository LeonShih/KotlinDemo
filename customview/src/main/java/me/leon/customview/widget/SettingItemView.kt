package me.leon.customview.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.layout_setting_item.view.*
import me.leon.customview.R

class SettingItemView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : RelativeLayout(context, attrs) {
    init {
        var root = View.inflate(context, R.layout.layout_setting_item, this)
        initStyle(attrs)
    }

    private fun initStyle(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.SettingItemView)

        val rightImg = a.getResourceId(R.styleable.SettingItemView_setting_right_img, -1)
        val rightMore = a.getResourceId(R.styleable.SettingItemView_setting_right_more_icon, -1)
        val title = a.getString(R.styleable.SettingItemView_setting_name)
        val title2 = a.getResourceId(R.styleable.SettingItemView_setting_name, -1)
        val rightTxt = a.getString(R.styleable.SettingItemView_setting_right_text)
        val rightTxt2 = a.getResourceId(R.styleable.SettingItemView_setting_right_text, -1)
        val hideBorder = a.getBoolean(R.styleable.SettingItemView_setting_hide_border, false)
        vBorder.visibility = if (!hideBorder) View.VISIBLE else View.GONE
        if (rightImg != -1) setRightImg(rightImg)
        if (rightMore != -1) setRightMore(rightMore)
        if (title2 != -1) setName(title2)
        if (!title.isNullOrEmpty()) setName(title)

        if (rightTxt2 != -1) setRightTxt(title2)
        if (!rightTxt.isNullOrEmpty()) setRightTxt(title)
        a.recycle()
    }

    private fun setName(name: Int) {
        tvSettingName.setText(name)
    }

    private fun setName(name: String) {
        tvSettingName.setText(name)
    }

    fun setRightTxt(name: Int) {
        tvSettingContent.setText(name)
    }

    fun setRightTxt(name: String) {
        tvSettingContent.setText(name)
    }

    private fun setRightMore(rightMore: Int) {
        ivSettingRight.setImageResource(rightMore)

    }

    fun setRightImg(rightImg: Int) {
        ivSettingImg.setImageResource(rightImg)
    }
}