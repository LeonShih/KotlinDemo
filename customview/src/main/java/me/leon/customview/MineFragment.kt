package com.shiduai.mobile.lawyer.ui.history



import com.shiduai.mobile.lawyer.frame.BFragment
import kotlinx.android.synthetic.main.fragment_history.*
import me.leon.customview.R

/**
 * <p>description：</p>
 * <p>author：Leon</p>
 * <p>date：2019/2/14 0014</p>
 * <p>e-mail：deadogone@gmail.com</p>
 *
 */
class MineFragment : BFragment() {
    override fun onViewInit() {
        tv.text = "我的"

    }

    override fun bindLayout(): Int = R.layout.fragment_history
}