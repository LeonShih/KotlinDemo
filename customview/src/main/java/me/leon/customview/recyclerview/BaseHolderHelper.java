package me.leon.customview.recyclerview;


import android.view.View;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;


public class BaseHolderHelper extends RecyclerView.ViewHolder {
    protected View itemView;

    public BaseHolderHelper(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public <T extends View> T getView(@IdRes int id) {

        return ((T) itemView.findViewById(id));
    }

    public BaseHolderHelper setText(@IdRes int id, String text) {
        TextView tv = getView(id);
        tv.setText(text);
        return this;
    }

    public BaseHolderHelper setText(@IdRes int id, @StringRes int strId) {
        TextView tv = getView(id);
        tv.setText(strId);
        return this;
    }

    public BaseHolderHelper setVisiable(@IdRes int id, boolean visiable) {
        getView(id).setVisibility(visiable ? View.VISIBLE : View.GONE);
        return  this;
    }

    public BaseHolderHelper setOnClickListener(@IdRes int id, View.OnClickListener listener) {
        getView(id).setOnClickListener(listener);
        return  this;
    }
}
