package me.leon.customview

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.shiduai.mobile.lawyer.frame.BActivity
import com.shiduai.mobile.lawyer.ui.history.HistoryFragment
import com.shiduai.mobile.lawyer.ui.history.MineFragment
import com.shiduai.mobile.lawyer.ui.history.VideoConsultFragment
import com.shiduai.mobile.lawyer.widget.IndicatorView
import kotlinx.android.synthetic.main.activity_main.*
import me.leon.customview.recyclerview.AbsAdapter
import me.leon.customview.recyclerview.BaseHolderHelper

class MainActivity : BActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        StatusBarHelper.setImmerse(this)
        StatusBarHelper.setDarkFontStatusBar(this)
//        StatusBarHelper.setFullScreen(this)


        bottom.apply {
            containerId = R.id.flContainer
            isHideText = false
            setTitleBeforeAndAfterColor("#999999", "#ff5d5e")
            addItem(VideoConsultFragment::class.java, "视频咨询",
                    R.mipmap.comui_tab_home, R.mipmap.comui_tab_home_selected)
            addItem(HistoryFragment::class.java, "历史记录",
                    R.mipmap.comui_tab_message, R.mipmap.comui_tab_message_selected)
            addItem(MineFragment::class.java, "我的",
                    R.mipmap.comui_tab_person, R.mipmap.comui_tab_person_selected)
        }.build()

        val layoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.HORIZONTAL
        }
        rv?.layoutManager = layoutManager

        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(rv)

        rv.adapter = object : AbsAdapter<String>(listOf("1","2","3"), android.R.layout.simple_list_item_1) {
            override fun convert(p0: BaseHolderHelper?, p1: String?, p2: Int) {

                p0?.setText(android.R.id.text1,p1)

                Log.d("recycler", "$p2")
            }
        }


        indicator.setUpWithRecyclerView(rv)
        indicator.setListener(object : IndicatorView.PositionChangeListener {
            override fun onPositionChange(old: Int, new: Int) {
                Log.d("recycler", "$new")
            }
        })
    }
}
