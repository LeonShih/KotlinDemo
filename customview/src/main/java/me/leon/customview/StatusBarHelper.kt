package me.leon.customview

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.os.Build

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager

import androidx.annotation.ColorInt
import androidx.annotation.ColorRes


/**
 * @author Leon
 * @data 2018-1-12
 * @desc 状态栏工具类
 */

object StatusBarHelper {
    private val COLOR_DEFAULT = Color.parseColor("#40000000")

    /**
     * 全屏
     *
     * @param activity
     */
    @TargetApi(19)
    fun setFullScreen(activity: Activity) {
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        //   activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);//下拉不消失
    }

    /**
     * 布局侵入状态栏
     *
     * @param activity
     */
    @TargetApi(19)
    fun setImmerse(activity: Activity) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            val window = activity.window
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            val contentView = window.decorView.findViewById<ViewGroup>(Window.ID_ANDROID_CONTENT)
            contentView.getChildAt(0).fitsSystemWindows = false
        }

    }

    /**
     * 设置状态栏颜色  21以下无法设置
     *
     * @param activity
     * @param color
     */
    fun setStatusBarColor(activity: Activity, @ColorRes color: Int) {
        val window = activity.window
        val resources = activity.resources

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(color)
        } else {
            addFakeStatusBar(activity, color)
        }

    }

    /**
     * @param activity
     * @desc 6.0以上原生支持, 以下兼容, 改变背状态栏字体颜色
     */

    @TargetApi(19)
    fun setDarkFontStatusBar(activity: Activity) {

        //当前手机版本为6.0及以上
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            activity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

            //activity.getWindow().setStatusBarColor(activity.getResources().getColor(statusColor));
            return
        } else if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT < 23) { //兼容5.x
            activity.window.statusBarColor = COLOR_DEFAULT
        }

        //手机版本为4.4
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            addFakeStatusBarColorInt(activity, COLOR_DEFAULT)
        }
    }

    @TargetApi(23)
    fun clearDarkFontFlag(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val decorView = activity.window.decorView
            val origin = decorView.systemUiVisibility
            decorView.systemUiVisibility = origin and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        }
    }

    /**
     * 添加fakeStatusBar 兼容Api 19及以下
     *
     * @param activity
     * @param color
     */
    fun addFakeStatusBar(activity: Activity, @ColorRes color: Int) {
        val resources = activity.resources

        val window = activity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        val systemContent = activity.findViewById<ViewGroup>(android.R.id.content)

        val statusBarView = View(activity)
        val lp = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                getStatusBarHeight(activity))
        statusBarView.setBackgroundColor(resources.getColor(color))

        systemContent.getChildAt(0).fitsSystemWindows = true

        systemContent.addView(statusBarView, 0, lp)
    }

    fun addFakeStatusBarColorInt(activity: Activity, @ColorInt color: Int) {

        val window = activity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        val systemContent = activity.findViewById<ViewGroup>(android.R.id.content)

        val statusBarView = View(activity)
        val lp = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                getStatusBarHeight(activity))
        statusBarView.setBackgroundColor(color)

        systemContent.getChildAt(0).fitsSystemWindows = true

        systemContent.addView(statusBarView, 0, lp)
    }


    /**
     * 增加View的paddingTop,增加的值为状态栏高度 (智能判断，并设置高度)
     */
    fun setPaddingSmart(context: Context, view: View) {
        if (Build.VERSION.SDK_INT >= 19) {
            val lp = view.layoutParams
            if (lp != null && lp.height > 0) {
                lp.height += getStatusBarHeight(context)//增高
            }
            view.setPadding(view.paddingLeft, view.paddingTop + getStatusBarHeight(context),
                    view.paddingRight, view.paddingBottom)
        }
    }

    /**
     * 获取状态栏高度
     *
     * @param activity
     * @return
     */
    fun getStatusBarHeight(activity: Context): Int {
        var result = 0
        val resourceId = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = activity.resources.getDimensionPixelSize(resourceId)
        }
        return result

    }


}
