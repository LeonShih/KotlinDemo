package com.shiduai.mobile.lawyer.frame

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import me.leon.customview.StatusBarCompact
import me.leon.customview.StatusBarHelper


/**
 * <p>description：top base Activity</p>
 * <p>author：Leon</p>
 * <p>date：2019/2/14 0014</p>
 * <p>e-mail：deadogone@gmail.com</p>
 *
 */
open class BActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenAutoAdapt(360f)

//        StatusBarCompact.compat(this, Color.WHITE)
//        StatusBarHelper.setDarkFontStatusBar(this)

    }

    /**
     * 屏幕宽度自动适配
     * @param designWidth 设计图宽度
     */
    private fun screenAutoAdapt(designWidth: Float) {

        val displayMetrics = resources.displayMetrics
        val density = displayMetrics.widthPixels / designWidth

        displayMetrics.density = density
        displayMetrics.densityDpi = (displayMetrics.density * 160).toInt()
        displayMetrics.scaledDensity = density

    }
}